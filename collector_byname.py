#!/usr/bin/python

from pymongo import MongoClient
import gridfs
import datetime
import sys
import os
import time

if(3 != len(sys.argv)):
	print 'Usage: ' + sys.argv[0] + ' Dir Duration'
	print 'Dir is the directory you want to scan for files'
	print 'Duration is the length of each file'
	sys.exit(1)

# final is the name of the DB
db = MongoClient().final

# StationAudio is the name of the collection
collection = db.StationAudio

# parses the file name into channel_name, date and time

def parseName(fname_full):
	channelStr, dateStr, timeStr = fname_full.split('_')
	timeStr, extention = timeStr.split('.')
	f_year, f_month, f_day = dateStr.split('-')
	f_hour, f_minute, f_second, f_millisecond, f_timezone = timeStr.split('-')
	
	start_time = datetime.datetime(int(f_year),int(f_month),int(f_day),int(f_hour),int(f_minute),int(f_second))
	interval = datetime.timedelta(seconds=int(sys.argv[2]))
	end_time = start_time + interval

	return channelStr, dateStr, start_time.strftime("%H-%M-%S"), end_time.strftime("%H-%M-%S")

for dirname, dirnames, filenames in os.walk(sys.argv[1]):
    for filename in filenames:
        c_file = os.path.join(dirname, filename)

        date_today = time.strftime("%d/%m/%Y")
        statinfo = os.stat(c_file)
        station_freq, date_today, start_time, end_time = parseName(filename)
        
        fs = gridfs.GridFS(db)

        data = open(c_file)
        new_flac_file = fs.put(data)
        data.close

        rec = {	"date": date_today, "start_time": start_time, "end_time": end_time, "station": station_freq, "flacfile": new_flac_file }

        collection.insert(rec)

        os.unlink(c_file)
        print 'File added to db: ' + c_file

sys.exit(0)