#!/bin/bash

while [ -f "lock" ]
do
	jack_rec -f $1_`date +%Y-%m-%d_%H-%M-%S`-000-0000.wav -d $2 -b 16 system:capture_$3
done
