#!/usr/bin/python
import datetime
import sys
import os
import time
import wave

fullStr=sys.argv[3]
channelStr, dateStr, timeStr = fullStr.split('_')
f_year, f_month, f_day = dateStr.split('-')
f_hour, f_minute, f_second, f_millisecond, f_timezone = timeStr.split('-')

a = datetime.datetime(int(f_year),int(f_month),int(f_day),int(f_hour),int(f_minute),int(f_second))
b = datetime.timedelta(seconds=int(sys.argv[2]))

c = a

lengthCmd = os.popen('soxi -D ' + sys.argv[3]).read()

length = lengthCmd.split('.')[0]

for x in range(0, int(length) + 1):
	newName = channelStr + '_' + str(c.year) + '-' + str(c.month).zfill(2) + '-' + str(c.day).zfill(2) + '_' + str(c.hour).zfill(2) + '-' + str(c.minute).zfill(2) + '-' + str(c.second).zfill(2) + '-' + str(f_millisecond) + '-0700'
	os.system('sox ' + sys.argv[3] + ' ' + sys.argv[1] + '/' + newName + '.flac trim ' + str(x*int(sys.argv[2])) + ' ' + sys.argv[2])
	c = c + b
