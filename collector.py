#!/usr/bin/python

from pymongo import MongoClient
import gridfs
import datetime
import sys
import os
import time


db = MongoClient().final
collection = db.StationAudio

while (1):
	for dirname, dirnames, filenames in os.walk(sys.argv[1]):
	    for filename in filenames:
	        c_file = os.path.join(dirname, filename)
	        date_today = time.strftime("%d/%m/%Y")
	        statinfo = os.stat(c_file)
	        start_time = int(statinfo.st_ctime)
	        end_time = start_time + int(sys.argv[2])
	        fs = gridfs.GridFS(db)
	        data = open(c_file)
	        new_flac_file = fs.put(data)
	        data.close

	        rec = {	"date": date_today, "start_time": start_time, "end_time": end_time, "channelID": 0, "channelName": "no channel", "flacfile": new_flac_file }

	        collection.insert(rec)

	        os.unlink(c_file)
	# This sleep exists so that we don't run through empty
	# loops when there are no
	time.sleep(1)
