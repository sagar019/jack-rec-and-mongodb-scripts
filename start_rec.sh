#!/bin/bash

ch1=91.2
ch2=87.3
ch3=98.4
ch4=100.1
ch5=101.1
ch6=99.9

line[1]=$ch1.L
line[2]=$ch1.R
line[3]=$ch2.L
line[4]=$ch2.R
line[5]=$ch3.L
line[6]=$ch3.R
line[7]=$ch4.L
line[8]=$ch4.R
line[9]=$ch5.L
line[10]=$ch5.R
line[11]=$ch6.L
line[12]=$ch6.R

touch lock

for i in {1..12}
do
	bash ./rec.sh $1/${line[$i]} $2 $i &
done
