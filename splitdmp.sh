#!/bin/sh

TMPDIR=`mktemp -dt rmm.XXXXXXXXXX` 
echo $TMPDIR

for i in `ls *.flac`;
do
	python /home/adentify/Sagar/split.py $TMPDIR $1 $i
done

python /home/adentify/Sagar/add2mongo.py $TMPDIR $1

rm -r /tmp/rmm.*
